let introImg;
let introTimeout = 200;
let isIntro = true;
let introSprite;
let introGroup;


// sounds
let gameMusic;
let startGameMusic = false;

// game sounds
let sound1;
let sound2;
let sound3;


// speech recognition
let speechRecognizer;
let isSpeechRecoStarted = false;

function preload() {
  loadIntro();
  loadSounds();
}

function setup() {
  createCanvas(800, 800);
  speechRecoLoad();

}

function draw() {
  if (isIntro) {
    intro();
  } else {
    launchGame();
  }
  // if (frameCount % 200 === 0 ) {
  //   console.log('test : ', speechRecognizer);
  // }
  // if (speechRecognizer.rec.onspeechend) {
  //   startSpeechReco(true, false);
  // }
}

// reset all game data
function reset() {
    startGameMusic = false;
}

// loading functions
function loadIntro() {
  introImg = loadImage('assets/spyman-a-week-a-game.png');
  introGroup = new Group();
}

function loadSounds() {
  // game music
  soundFormats('m4a', 'ogg');
  gameMusic = loadSound('assets/sounds/gameMusic');
  gameMusic.setVolume(0.1);

  soundFormats('mp3', 'ogg');
  sound1 = loadSound('assets/sounds/mp3_game/sound1.mp3');
  sound1.setVolume(0.1);
  sound2 = loadSound('assets/sounds/mp3_game/sound2.mp3');
  sound2.setVolume(0.1);
  sound3 = loadSound('assets/sounds/mp3_game/sound3.mp3');
  sound3.setVolume(0.1);
}

function speechRecoLoad() {
  console.log('navigator : ', navigator.lang);
  let lang = navigator.lang || 'en-US';
  speechRecognizer = new p5.SpeechRec(lang);
}

// draw functions
function launchGame() {
  if (!startGameMusic) {
    startGameMusic = true;
    gameMusic.loop();
  }

  if (!isSpeechRecoStarted) {
    isSpeechRecoStarted = true;
    startSpeechReco(true, false);
  }


  playSounds();
  pauseMusic();
}

function intro() {
  introSprite = createSprite(width/2, height/2);
	introSprite.addImage(introImg);
  introGroup.add(introSprite);
  drawSprites(introGroup);
  if (frameCount%introTimeout === 0) {
    isIntro = false;
    clear();
    background(0);
  }
}

// controller

function playSounds() {
  if (keyWentDown(88)) {
    sound1.play();
  } else if (keyWentDown(67)) {
    sound2.play();
  } else if (keyWentDown(87)) {
    sound3.play();
  }
}

function pauseMusic() {
  // click on f2 key to stop and relaunch music
  if (keyWentDown(113)) {
    if (gameMusic.isPlaying()) {
      gameMusic.stop();
    } else {
      gameMusic.loop();
    }
  }
}

function startSpeechReco(continuous, interim) {
  speechRecognizer.start(continuous, interim);
  speechRecognizer.onResult = test;

  function test() {
    console.log('speech : ', speechRecognizer);
  }
}
